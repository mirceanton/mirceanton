# Hi there 👋

<a href="https://github.com/mirceanton">
  <img align="center" src="https://github-readme-stats.vercel.app/api?username=mirceanton&show_icons=true&theme=onedark&hide=stars" />
</a>

## I'm Mircea Anton, a Computer Science student from Bucharest, Romania

- 🔭 I’m currently working on...
  - ... implementing the entirety of my HomeLab via IaC
  - ... **My health and fitness** 💪
- 🌱 I’m currently learning...
  - to get my C.K.A. & C.K.A.D.
- 👯 I’m looking to collaborate... **with other open-source devs**
- 📫 How to reach me ... [**shoot me a DM**](https://mirceanton.com/contact)
- ⚡ Fun fact ... **I ❤️ iced latte**
